package com.example.listview92;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import Modelo.AlumnosDb;

public class Aplicacion extends Application {
    public static ArrayList<Alumno> alumnos;
    private MiAdaptador adaptador;

    private AlumnosDb alumnosDb;

    public ArrayList<Alumno> getAlumnos(){ return alumnos; }
    public MiAdaptador getAdaptador(){ return adaptador; }

    @Override
    public void onCreate(){
        super.onCreate();
        alumnosDb = new AlumnosDb(getApplicationContext());
        alumnosDb.openDataBase(); // Abrir la base de datos antes de acceder a ella
        alumnos = alumnosDb.allAlumnos();
        adaptador = new MiAdaptador(alumnos, this);
        Log.d("", "onCreate: tamaño array list " + alumnos.size());
    }

    public void agregarAlumno(Alumno alumno) {
        Alumno nuevoAlumno = new Alumno(alumno.getMatricula(), alumno.getNombre(), alumno.getImg(), alumno.getCarrera());
        alumnos.add(nuevoAlumno);
        adaptador.notifyDataSetChanged();
    }
}
