package com.example.listview92;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import Modelo.AlumnosDb;

public class AlumnoAlta extends AppCompatActivity {

    private Button btnGuardar, btnRegresar;
    private Alumno alumno;
    private EditText txtNombre, txtMatricula, txtCarrera;
    private ImageView imgAlumno;
    private TextView lblImagen;
    private int posicion;
    private AlumnosDb alumnosDb;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_alumno_alta);
        btnGuardar = (Button) findViewById(R.id.btnSalir);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        txtMatricula = (EditText) findViewById(R.id.txtMatricula);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtCarrera = (EditText) findViewById(R.id.txtCarrera);
        imgAlumno = (ImageView) findViewById(R.id.imgAlumno);

        Bundle bundle = getIntent().getExtras();
        alumno = (Alumno) bundle.getSerializable("alumno");
        posicion = bundle.getInt("posicion", posicion);

        if (posicion >= 0) {
            if (alumno != null) {
                txtMatricula.setText(alumno.getMatricula());
                txtNombre.setText(alumno.getNombre());
                txtCarrera.setText(alumno.getCarrera());
                imgAlumno.setImageResource(alumno.getImg());
            }
        }

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alumno == null || txtMatricula == null) {
                    alumno = new Alumno();
                    alumno.setCarrera(txtCarrera.getText().toString());
                    alumno.setMatricula(txtMatricula.getText().toString());
                    alumno.setNombre(txtNombre.getText().toString());
                    alumno.setImg(R.drawable.us);

                    if (validar()) {
                        alumnosDb = new AlumnosDb(getApplicationContext());
                        Aplicacion.alumnos.add(alumno);
                        alumnosDb.insertAlumno(alumno);

                        setResult(Activity.RESULT_OK);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "Faltó capturar datos", Toast.LENGTH_SHORT).show();
                        txtMatricula.requestFocus();
                    }
                }

                if (posicion >= 0) {
                    alumno.setMatricula(txtMatricula.getText().toString());
                    alumno.setNombre(txtNombre.getText().toString());
                    alumno.setCarrera(txtCarrera.getText().toString());

                    Aplicacion app = (Aplicacion) getApplication();
                    app.getAlumnos().get(posicion).setMatricula(alumno.getMatricula());
                    app.getAlumnos().get(posicion).setNombre(alumno.getNombre());
                    app.getAlumnos().get(posicion).setCarrera(alumno.getCarrera());
                    alumnosDb = new AlumnosDb(getApplicationContext());
                    alumnosDb.updateAlumno(alumno);

                    Toast.makeText(getApplicationContext(), "Se modificó con éxito :D", Toast.LENGTH_SHORT).show();
                }
                finish();
            }
        });



        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    private boolean validar(){
        boolean exito = true;
        Log.d("nombre", "validar: " + txtNombre.getText());
        if (txtNombre.getText().toString().isEmpty()) exito = false;
        if (txtMatricula.getText().toString().isEmpty()) exito = false;
        if (txtCarrera.getText().toString().isEmpty()) exito = false;

        return exito;
    }
}
